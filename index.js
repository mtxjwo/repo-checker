const configurationClass = require('./libs/utils/configurations.class');
const logClass = require('./libs/utils/log.class');
const fs = require('fs');
const git = require('simple-git')();
const gitPromise = require('simple-git/promise');
const exec = require('child_process').exec;
const mkdirp = require('mkdirp');
const process = require('process');

class RepositoryChecker {
	constructor() {
		this.configuration = new configurationClass();
		this.log = new logClass(this.configuration);
		this.isUpdating = false;
		this.missingRepos = [];
		this.runningRepoIds = {};
		this.runningRepos = {};
		this.installingRepos = {};
		this.log.generic('Oracle initialising...');
		this.initialiseAccessControl();
		setInterval(() => {
			if (!this.isUpdating) {
				this.isUpdating = true;
				this.checkRepoExistCloneMissing();
			}
		}, this.configuration.application.repoUpdateFrequency);
	}

	initialiseAccessControl() {
		git.addConfig('user.email', this.repoEmail);
		git.addConfig('user.name', this.repoUsername);
	}

	checkIfRepositoryExist(repoPath, callback = null) {
		const path = repoPath;
		return new Promise((resolve) => {
			fs.access(path, fs.F_OK, (err) => {
				if (!callback) {
					resolve();
				}
				if (err) {
					callback(err);
					resolve();
					return;
				}
				callback(true);
				resolve();
			});
		});
	}

	async checkRepoExistCloneMissing() {
		for (const repo of this.configuration.application.repoList) {
			let doesRepoExist = false;
			let repoIndex = this.configuration.application.repoList.indexOf(
				repo
			);
			await this.checkIfRepositoryExist(
				this.configuration.application.repoClonedDir +
					'/' +
					this.configuration.application.repoPaths[repoIndex],
				(result) => {
					doesRepoExist = result;
				}
			);
			if (doesRepoExist !== true) {
				this.missingRepos.push(repo);
			} else if (doesRepoExist === true) {
				let requireReInstallation = false;
				await this.repoPull(repo, (result) => {
					if (result) {
						requireReInstallation = true;
					}
				});
				if (requireReInstallation) {
					await this.killRepo(repo);
					await this.installRepo(repo);
				} else {
					this.runRepo(repo);
				}
			}
		}
		for (const missingRepo of this.missingRepos) {
			await this.repoClone(missingRepo);
			await this.installRepo(missingRepo);
			this.runRepo(missingRepo);
		}
		this.missingRepos = [];
		this.isUpdating = false;
	}

	formProcessArgs(repoName) {
		const repoIndex = this.configuration.application.repoList.indexOf(
			repoName
		);
		if (
			typeof this.configuration.application.repoArgs[repoIndex] !==
				'object' &&
			typeof this.configuration.application.repoArgs[repoIndex] !==
				'string'
		) {
			return;
		}
		if (this.configuration.application.repoArgs[repoIndex].length <= 0) {
			return;
		}
		if (
			typeof this.configuration.application.repoArgs[repoIndex] ===
			'string'
		) {
			return this.configuration.application.repoArgs[repoIndex];
		}
		return this.configuration.application.repoArgs[repoIndex].join(' ');
	}

	runRepo(repoName) {
		let repoIndex = this.configuration.application.repoList.indexOf(
			repoName
		);
		if (this.configuration.application.repoToRun[repoIndex] === false) {
			return;
		}
		if (
			repoName in this.runningRepos &&
			this.runningRepos[repoName] !== undefined
		) {
			return;
		}

		this.log.generic('Running instance of: ' + repoName);
		this.runningRepos[repoName] = exec(
			'bash ' +
				this.configuration.application.repoClonedDir +
				'/' +
				repoName +
				'/run.sh ' +
				this.formProcessArgs(repoName)
		);
		this.runningRepos[repoName].stdout.on('data', async (data) => {
			if (data.length < 2) {
				return;
			}
			if (
				repoName in this.runningRepoIds === false ||
				this.runningRepoIds[repoName] === undefined
			) {
				this.log.generic(
					'ID (' + data + ') assigned to instance: ' + repoName
				);
				this.runningRepoIds[repoName] = data;
			}
			console.log(data);
		});
		this.runningRepos[repoName].stderr.on('data', (data) => {
			if (data.length < 2) {
				return;
			}
			this.log.generic(
				'Error detected: ' + typeof data === 'object'
					? JSON.stringify(data)
					: data
			);
		});
	}

	/**
	 * Send kill signal to app
	 *
	 * @param {*} repoName
	 */
	killRepo(repoName) {
		return new Promise((resolve) => {
			let repoIndex = this.configuration.application.repoList.indexOf(
				repoName
			);
			if (
				this.configuration.application.repoToKill[repoIndex] === false
			) {
				resolve();
				return;
			}
			this.log.generic(
				'Killing repo (' +
					repoName +
					') with PID: ' +
					this.runningRepoIds[repoName]
			);
			const ex = exec(
				'bash ' +
					this.configuration.application.repoClonedDir +
					'/' +
					repoName +
					'/kill.sh ' +
					this.runningRepoIds[repoName]
			);
			ex.stdout.on('data', (data) => {
				if (data.length < 2) {
					return;
				}
				if (data.indexOf('APP_KILL_SUCCEEDED') > -1) {
					this.log.generic('[Killing] : ' + data);
					this.runningRepoIds[repoName] = undefined;
					this.runningRepos[repoName] = undefined;
					resolve();
				} else {
					this.log.generic('[Killing] : ' + data);
				}
				resolve();
			});
		});
	}

	killAll() {
		this.log.generic('KILLING ALL');
		return new Promise((resolve) => {
			const runningRepoKeys = Object.keys(this.runningRepos);
			if (runningRepoKeys.length <= 0) {
				resolve();
				return;
			}
			let repoKilled = 0;
			let repoCount = runningRepoKeys.length;
			for (const r of runningRepoKeys) {
				const repoName = r;
				this.log.generic(
					'Killing repo (' +
						repoName +
						') with PID: ' +
						this.runningRepoIds[repoName]
				);
				const ex = exec(
					'bash ' +
						this.configuration.application.repoClonedDir +
						'/' +
						repoName +
						'/kill.sh ' +
						this.runningRepoIds[repoName]
				);
				ex.stdout.on('data', (data) => {
					if (data.length < 2) {
						return;
					}
					if (data.indexOf('APP_KILL_SUCCEEDED') > -1) {
						this.log.generic('[Killing] : ' + data);
						this.runningRepoIds[repoName] = undefined;
						this.runningRepos[repoName] = undefined;
						repoKilled++;
					} else {
						this.log.generic('[Killing] : ' + data);
					}
				});
			}
			setInterval(() => {
				if (repoKilled >= repoCount) {
					this.log.generic('DONE');
					resolve();
				}
			}, 1000);
		});
	}

	installRepo(repoName) {
		return new Promise((resolve) => {
			let isUnlocked = false;
			let repoIndex = this.configuration.application.repoList.indexOf(
				repoName
			);
			if (
				this.configuration.application.repoToInstall[repoIndex] ===
				false
			) {
				resolve();
				return;
			}
			const installConfig = this.retrieveInstallConfiguration(repoName);
			this.log.generic(
				'Installing repo (' +
					repoName +
					') with params: ' +
					installConfig
			);
			const ex = exec(
				'bash ' +
					this.configuration.application.repoClonedDir +
					'/' +
					repoName +
					'/install.sh ' +
					installConfig
			);
			ex.stdout.on('data', (data) => {
				if (data.length < 2) {
					return;
				}
				if (data.indexOf('CLOUD_INSTALLATION_UNLOCK') > -1) {
					isUnlocked = true;
				}
				if (data.indexOf('CLOUD_INSTALLATION_SUCCEDED') > -1) {
					this.log.generic('[Installation] : COMPLETE');
					resolve();
					return;
				} else {
					this.log.generic('[Installation] : ' + data);
					if (data.indexOf('audited') > -1 && isUnlocked) {
						this.log.generic('[Installation] : COMPLETE');
						resolve();
						return;
					}
				}
				return;
			});
		});
	}

	repoPull(repoName, callback = null) {
		return new Promise((resolve) => {
			const remoteUrl = this.resovleGitRepoAuthorisedUrl(repoName);
			const repoCheckout =
				this.configuration.application.repoCheckoutBranch === ''
					? 'master'
					: this.configuration.application.repoCheckoutBranch;
			gitPromise(
				this.configuration.application.repoClonedDir + '/' + repoName
			)
				.pull(remoteUrl, repoCheckout, { '--force': true })
				.then((result) => {
					if (result.summary.changes != '0') {
						this.log.generic('New update applied to: ' + repoName);
						if (callback) {
							callback(true);
						}
					}
					resolve();
				})
				.catch((err) => {
					this.log.error('Error fetching repository: ' + repoName);
					if (callback) {
						callback(false);
					}
					resolve();
				});
		});
	}

	repoClone(repoName, callback = null) {
		const repoCheckout =
			this.configuration.application.repoCheckoutBranch === ''
				? 'master'
				: this.configuration.application.repoCheckoutBranch;
		this.log.generic(
			'Cloning repository: ' +
				repoName +
				' in branch ' +
				repoCheckout +
				'...'
		);
		const remoteUrl = this.resovleGitRepoAuthorisedUrl(repoName);
		return new Promise((resolve) => {
			mkdirp.sync(this.configuration.application.repoClonedDir);
			mkdirp.sync(
				this.configuration.application.repoClonedDir + '/' + repoName
			);
			gitPromise(
				this.configuration.application.repoClonedDir + '/' + repoName
			)
				.clone(
					remoteUrl,
					this.configuration.application.repoClonedDir +
						'/' +
						repoName
				)
				.then(() => {
					this.log.generic(
						'Cloning repository succeeded: ' + repoName
					);
					this.log.generic(
						'Retrieving selecte branch: ' + repoCheckout
					);
					return gitPromise(
						this.configuration.application.repoClonedDir +
							'/' +
							repoName
					).pull(remoteUrl, repoCheckout, {
						'--force': true,
					});
				})
				.then((result) => {
					this.log.generic('Branch switch result:  ' + result);
					this.log.generic('Switching to branch ' + repoCheckout);
					return gitPromise(
						this.configuration.application.repoClonedDir +
							'/' +
							repoName
					).checkoutBranch(repoCheckout, 'origin/' + repoCheckout);
				})
				.then((result) => {
					this.log.generic('Branch checkout result:  ' + result);
					if (callback) {
						callback(true);
					}
					resolve();
				})
				.catch((err) => {
					console.log(err);
					this.log.error('Error cloning repository: ' + repoName);
					if (callback) {
						callback(false);
					}
					resolve();
				});
		});
	}

	resolveGitCridentials() {
		const USER = this.configuration.application.repoUsername;
		const PASS = this.configuration.application.repoPassword;
		return USER + ':' + PASS;
	}

	resovleGitRepoUrl(repoName) {
		return (
			this.repoProtocol +
			this.configuration.application.repoHost +
			'/' +
			this.configuration.application.repoOrigin +
			repoName
		);
	}

	resovleGitRepoAuthorisedUrl(repoName) {
		if (
			this.repoProtocol === 'http://' ||
			this.repoProtocol === 'https://'
		) {
			return (
				this.repoProtocol +
				this.repoUsername +
				':' +
				this.repoPassword +
				'@' +
				this.configuration.application.repoHost +
				'/' +
				this.configuration.application.repoOrigin +
				repoName
			);
		}
		return (
			this.repoProtocol +
			this.repoUsername +
			':' +
			this.repoPassword +
			'@' +
			this.configuration.application.repoHost +
			':' +
			this.configuration.application.repoOrigin +
			repoName
		);
	}

	retrieveInstallConfiguration(repoName) {
		const installationConfigurationRaw = fs.readFileSync(
			'./install/' + repoName + '.conf.json',
			{
				encoding: 'utf8',
				flag: 'r',
			}
		);
		const installConfig = JSON.parse(installationConfigurationRaw);
		const objectKeys = Object.keys(installConfig);
		return objectKeys
			.map((k) => {
				return installConfig[k];
			})
			.join(' ');
	}

	get repoProtocol() {
		return this.configuration.application.repoProtocol;
	}

	get repoUsername() {
		return this.configuration.application.repoUsername;
	}

	get repoPassword() {
		return this.configuration.application.repoPassword;
	}

	get repoEmail() {
		return this.configuration.application.repoEmail;
	}
}

const app = new RepositoryChecker();

process.on('SIGTERM', () => {
	app.killAll().then(() => {
		console.log('Process killed.');
		process.exit(0);
	});
});

process.on('SIGQUIT', () => {
	app.killAll().then(() => {
		console.log('Process killed.');
		process.exit(0);
	});
});

process.on('SIGINT', () => {
	app.killAll().then(() => {
		console.log('Process killed.');
		process.exit(0);
	});
});
