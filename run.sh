#!/bin/bash
DIR_PATH=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

git update-index --skip-worktree $DIR_PATH/conf/app.conf.js

# create all extra directories required
[ ! -d "$DIR_PATH/logs" ] && mkdir $DIR_PATH/logs
[ ! -d "$DIR_PATH/logs/generic" ] && mkdir $DIR_PATH/logs/generic
[ ! -d "$DIR_PATH/logs/errors" ] && mkdir $DIR_PATH/logs/errors
[ ! -d "/var/www/git" ] && mkdir -p /var/www/git
[ ! -d "$DIR_PATH/install" ] && mkdir -p $DIR_PATH/install

# invoke git to ignore the changes in the install directory
toSkipFiles="git ls-files -v ./install/ |grep '^S'"
git update-index --skip-worktree "$@"; 

# execute the repositories
repoCheckerPid=-1;
node $DIR_PATH/index.js &
repoCheckerPid=$!
trap 'echo "killing repository checker process (PID: $repoCheckerPid)"; pkill -9 $repoCheckerPid; exit' SIGINT

while : 
do
  sleep 1
done
