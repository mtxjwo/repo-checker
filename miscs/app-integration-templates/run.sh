#!/bin/bash
DIR_PATH=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

# Perform application exeuction here
cd $DIR_PATH/
nodejs $DIR_PATH/server/server.js "param-argument-1" "param-argument-2" &

# Send the applicaiton's PID to checker
echo $!
exit 1