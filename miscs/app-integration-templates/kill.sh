#!/bin/bash

# Terminate the app before performing installation of changes
echo $1
kill -9 $1
wait
echo "APP_KILL_SUCCEEDED"
exit 1