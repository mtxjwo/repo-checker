#!/bin/bash
DIR_PATH=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

# Prepare installation process
echo "Installing package...";
cd $DIR_PATH
echo "moving to $DIR_PATH...";
echo "CLOUD_INSTALLATION_UNLOCK";

# Perform your application installation scripts here
echo "Publishing example app...";
cp -R $DIR_PATH/distribution-files/* /var/www/html

# Send signal to checker to indicate installation completion
echo "CLOUD_INSTALLATION_SUCCEDED"
exit 1