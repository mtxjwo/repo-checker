const fs = require('fs');

module.exports = class log {
	constructor(configurations) {
		this.configurations = configurations;
		this.logQueueGeneric = [];
		this.logQueueError = [];

		this.writeStreamPaused = false;
		this.writeStreamFileName = this.createLogFileName();
		this.writeStreamGeneric = fs.createWriteStream(
			__dirname +
				'/../../' +
				this.configurations.application.generic_log_folder +
				'/' +
				this.writeStreamFileName,
			{ flags: 'a' }
		);
		this.writeStreamErrors = fs.createWriteStream(
			__dirname +
				'/../../' +
				this.configurations.application.error_log_folder +
				'/' +
				this.writeStreamFileName,
			{ flags: 'a' }
		);

		setInterval(() => {
			if (this.createLogFileName() !== this.writeStreamFileName) {
				this.writeStreamPaused = true;
				this.writeStreamGeneric.end();
				this.writeStreamErrors.end();
				this.writeStreamFileName = this.createLogFileName();
				this.writeStreamGeneric = fs.createWriteStream(
					this.configurations.application.generic_log_folder +
						'/' +
						this.writeStreamFileName,
					{ flags: 'a' }
				);
				this.writeStreamErrors = fs.createWriteStream(
					this.configurations.application.error_log_folder +
						'/' +
						this.writeStreamFileName,
					{ flags: 'a' }
				);
				this.writeStreamPaused = false;
			}
		}, 60000);

		setInterval(() => {
			if (
				this.logQueueGeneric.length > 0 &&
				this.writeStreamPaused === false
			) {
				this.print(this.logQueueGeneric[0]);
				this.writeToFile(true, this.logQueueGeneric[0]);
				this.logQueueGeneric.splice(0, 1);
			}
			if (
				this.logQueueError.length > 0 &&
				this.writeStreamPaused === false
			) {
				this.print('[ERROR]: ' + this.logQueueError[0]);
				this.writeToFile(false, this.logQueueError[0]);
				this.logQueueError.splice(0, 1);
			}
		}, 1);
	}

	generic(message) {
		this.logQueueGeneric.push(this.getLogTime() + message);
	}

	error(message) {
		this.logQueueError.push(this.getLogTime() + message);
	}

	endWriteStream() {
		this.writeStreamGeneric.end();
		this.writeStreamErrors.end();
	}

	print(message) {
		process.stdout.write(message);
	}

	getLogTime() {
		const time = new Date();
		return (
			'[' +
			time.toLocaleDateString('en-GB') +
			' ' +
			time.toLocaleTimeString('en-GB') +
			']: '
		);
	}

	createLogFileName() {
		const time = new Date();
		const t = time.toLocaleDateString('en-US').split('/');
		return t.join('-') + '.log';
	}

	writeToFile(genericLog, data) {
		if (genericLog) {
			this.writeStreamGeneric.write(data + '\n');
		} else {
			this.writeStreamErrors.write(data + '\n');
		}
	}
};
