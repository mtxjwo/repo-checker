const appConfigs = require('../../conf/app.conf.js');

module.exports = class configurations {
	constructor() {
		this.application = appConfigs;
	}
};
