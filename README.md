# Repository Checker

A lightweight CI/CD tool for projects. The tool supports installation, execution, and termination of applications in a remote server when changes are detected on chosen branches of integrated repositories in the app.

![workflow-diagra.png](https://bitbucket.org/mtxjwo/repo-checker/raw/4a119ae3ac6d31c50272d240b8590e243c2eae39/workflow-diagram.png)

### Install & Run

- Required dependency: NodeJS 10.x
- Recommended OS platform: Ubuntu (18.04)
- Install the package using npm install or npm i
- Configure the application configuration within "conf/app.conf.js"
- Add your application installation JSON files in the "install" folder.
- Start the Repository Checker by typing "bash run.sh"

### App Integration

Please integrate the following components to your application in order for Repository Checker to be able install, and manage your app.

- install.sh - Is executed once every change is detected.
- run.sh - Is executed after Install.sh succeeds.
- kill.sh - Is executed before an existing app is re/installed.

Copy the integration templates from miscs/app-integration-templates to your application and configure as required (see below - Your App's Install Configuration - for more information).

On first run, the checker will clone your apps and perform installation using your app's install.sh.

All installation processes for your app must be performed within install.sh.

On success installation, the app's run.sh is executed by the checker, the app's PID is stored for later use (app termination).

The checker will detect changes in your chosen branch of your selected repository. When changes are detected for an app, it is terminated by executing the app's kill.sh and re-installed and run with the updated files from the remote branch using install.sh and run.sh.

Important NOTE: some apps may not need to run (i.e. static page websites), only installation therefore run.sh & kill.sh are optional processes, see below for configuration instructions.

### Repo Checker Configuration

Below are the properties that you need to change in conf/app.conf.js to run the checker properly.

- repoProtocol: 'https://', # String - Valid options are: "http://" or "https://"
- repoHost: 'bitbucket.org', # String - The domain name of repository host - no trailing slashes, just the domain
- repoOrigin: 'git/', # String - User profile or project folder in host - include the trailing slash at the end
- repoUsername: 'git', # String - User that has access to the repository - recommended settings is READ only.
- repoPassword: 'repo-paassword', # String - User password
- repoList: ['my-app-repo','my-server-repo'], # String[] - List of repository to clone from repoHost
- repoPaths: [my-app-repo','my-server-repo'], # String[] - Clone the repository to which local folders
- repoArgs: [''], # String[] - Runtime arguments for each repository, keep it minimal - for further configurations use the installation configuration file instead (see below).
- repoClonedDir: '/root/repository-checker', # string - In which local directory should all the repository be cloned
- repoToInstall: [true], # Boolean[] - Whether to perform the installation (install.sh) of a repository
- repoToRun: [true], # Boolean[] - Whether to perform the run procedure (run.sh) of a repository
- repoToKill: [true], # Boolean[] - Whether to perform the kill procedure (kill.sh) of a repository
- repoUpdateFrequency: 30000, # Number - How often should the daemon check for repository updates from repoHost
- generic_log_folder: 'logs/generic', # String - Where must the daemon save its logs
- error_log_folder: 'logs/errors' # String - Where must the daemon save its error logs

### Your App's Install Configuration

Your app's install.conf.json will be used by the checker during the installation process. The keys and values in this file will be passed to install.sh in which you can use for whatever installation process you have implemented for your app. Ideally, the installation process should create your app's configuration file.

Separately, your app can also set its configuration properties using the argument parameters set in your checker's app.conf.js's repoArgs settings. However, this is not recommended since it is not manageable to create long trails of configuration parameters within the app.conf.js. If you have a long list of configuration for your application, use the install.conf.json strategy instead.

The install.conf.json will be included in your app's folder - this is just a template. Copy this file to the checker's install folder and rename it to your app's repository name i.e. website-app.conf.json (if your app's repository name is website-app). Then you can confiure this file for your application.

### Your App's Runtime Configuration

Your application's run.sh will be executed after the checker successfully installs/clones the changes from your chosen branch in your app's repository.

You can customise this script to your requirement. It is important however, not to change the core parts of the script incidcated by the comment lines.

It is not necessary to change the kill.sh script.

### Contributions Welcome & Licensing

- Please submit improvements to the application - include motivation and change log.
- MIT License
